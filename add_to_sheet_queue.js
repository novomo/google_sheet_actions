/*
    Tracker interactions queue
*/

// node modules
const Queue = require("bull");

// user defined modules

const { addTip, updateTip, deleteTip } = require("./tip_sheet");
const { addMasterTip, updateMasterTip } = require("./tip_master_sheet");
const IP = require("ip");
const upload_error = require("../node_error_functions/upload_error");

const { REDIS_PORT, REDIS_HOSTNAME } = require("../env");

// initiate queue
const addDataToSheet = new Queue(
  "addDataToSheet",
  (opts = {
    redis: {
      host: REDIS_HOSTNAME,
      port: REDIS_PORT,
    },
  })
);

// process jobs in queue
addDataToSheet.process(async (job) => {
  const { service, data, sheet } = job.data;
  //console.log(service, data, sheet);
  try {
    switch (service.toLowerCase()) {
      case "add_tip": {
        const response = await addTip(data, sheet, job.id);
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }
      case "update_tip": {
        const response = await updateTip(data, sheet, job.id);
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }

      case "delete_tip": {
        const response = await deleteTip(data, sheet, job.id);
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }

      case "add_master_tip": {
        const response = await addMasterTip(data, sheet, job.id);
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }
      case "update_master_tip": {
        const response = await updateMasterTip(data, sheet, job.id);
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }
      default: {
        if (response) {
          addDataToSheet.getJob(job.id).then(function (job) {
            return job.remove();
          });
        }
        return Promise.resolve({ added: true });
      }
      //t
    }
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Assigning task to sheet actions",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
    return Promise.reject(err);
  }
});

module.exports.addDataToSheet = addDataToSheet;
